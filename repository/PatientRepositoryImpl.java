package com.patientapp.repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.patientapp.exceptions.AadharNumberNotFoundException;
import com.patientapp.exceptions.ContactNotFoundException;
import com.patientapp.exceptions.DateNotFoundException;
import com.patientapp.exceptions.DiseaseNotFoundException;
import com.patientapp.exceptions.DoctorNotFoundException;
import com.patientapp.exceptions.PatientBedNumberNotFoundException;
import com.patientapp.model.Patient;

public class PatientRepositoryImpl implements IPatientRepository {
	private DbManager dbManager = new DbManager();
	private MongoCollection<Patient> collection = dbManager.getCollection();

	@Override
	public void addPatient(Patient patient) {
		collection.insertOne(patient);
		System.out.println("Patient with Name: " + patient.getPatientName() + " is successfully added.");
	}

	@Override
	public void updatePatient(String aadharNumber, String mobileno) throws AadharNumberNotFoundException {
		Patient patient = null;
		patient = collection.find(Filters.eq("_id", aadharNumber)).first();
		if (patient == null)
			throw new AadharNumberNotFoundException("Patient with given AadharNumber(" + aadharNumber + ") not found!");
		else {
			collection.updateOne(Filters.eq("_id", aadharNumber), Updates.set("contactNo", mobileno));
			System.out.println("Patient with Name: " + patient.getPatientName() + " is successfully updated.");
		}
	}

	@Override
	public void deletePatient(String aadharNumber) throws AadharNumberNotFoundException {
		Patient patient = null;
		patient = collection.find(Filters.eq("_id", aadharNumber)).first();
		if (patient == null)
			throw new AadharNumberNotFoundException("Patient with given AadharNumber(" + aadharNumber + ") not found!");
		else {
			collection.deleteOne(Filters.eq("_id", aadharNumber));
			System.out.println("Patient with Name: " + patient.getPatientName() + " is successfully Deleted.");
		}
	}

	@Override
	public List<Patient> findByBedNo(Integer patientBedNo) throws PatientBedNumberNotFoundException {
		List<Patient> patientByBedNumber = null;
		patientByBedNumber = collection.find(Filters.eq("patientBedNo", patientBedNo)).into(new ArrayList<Patient>())
				.stream().sorted((obj1, obj2)->obj1.getPatientName().compareTo(obj2.getPatientName()))
				.collect(Collectors.toList());
		if (patientByBedNumber == null || patientByBedNumber.isEmpty())
			throw new PatientBedNumberNotFoundException(
					"Patient with given BedNumber(" + patientBedNo + ") not found!");
		else {
			return patientByBedNumber;
		}
		// ------------------------------------------------------------------------------------------------------------------------
//		return collection.find(Filters.eq("patientBedNo", patientBedNo)).into(new ArrayList<Patient>()).stream().findFirst().get();
//		return collection.find().into(new ArrayList<Patient>()).stream()
//				.filter(patient->patient.getPatientBedNo() == patientBedNo)
//				.findFirst()
//				.orElseThrow(new PatientBedNumberNotFoundException("Patient with given BedNumber("+patientBedNo+") not found!"));
		// ------------------------------------------------------------------------------------------------------------------------
	}

	@Override
	public List<Patient> getAllPatients() {
		return collection.find().into(new ArrayList<Patient>()).stream()
				.sorted((obj1, obj2)->obj1.getPatientName().compareTo(obj2.getPatientName()))
				.collect(Collectors.toList());
	}

	@Override
	public List<Patient> findByDisease(String disease) throws DiseaseNotFoundException {
		List<Patient> patientByDisease = null;
		patientByDisease = collection.find(Filters.eq("disease", disease)).into(new ArrayList<Patient>())
				.stream().sorted((obj1, obj2)->obj1.getPatientName().compareTo(obj2.getPatientName()))
				.collect(Collectors.toList());
		if (patientByDisease == null || patientByDisease.isEmpty())
			throw new DiseaseNotFoundException("Patient with given Disease(" + disease + ") not found!");
		else {
			return patientByDisease;
		}
	}

	@Override
	public List<Patient> findByDoctorConsideration(String doctorName) throws DoctorNotFoundException {
		List<Patient> patientByDoctorConsideration = null;
		patientByDoctorConsideration = collection.find(Filters.eq("underConsideration", doctorName))
				.into(new ArrayList<Patient>())
				.stream().sorted((obj1, obj2)->obj1.getPatientName().compareTo(obj2.getPatientName()))
				.collect(Collectors.toList());
		if (patientByDoctorConsideration == null || patientByDoctorConsideration.isEmpty())
			throw new DoctorNotFoundException("No Patient are under(" + doctorName + ")!");
		else {
			return patientByDoctorConsideration;
		}
	}

	@Override
	public List<Patient> findByAdmitDate(String admitDate) throws DateNotFoundException {
		List<Patient> patientByByAdmitDate = null;
		patientByByAdmitDate = collection.find(Filters.eq("admitDate", admitDate)).into(new ArrayList<Patient>())
				.stream().sorted((obj1, obj2)->obj1.getPatientName().compareTo(obj2.getPatientName()))
				.collect(Collectors.toList());
		if (patientByByAdmitDate == null || patientByByAdmitDate.isEmpty())
			throw new DateNotFoundException("No Data Found for the Date : " + admitDate + ".");
		else {
			return patientByByAdmitDate;
		}
	}

	@Override
	public List<Patient> findByReleaseDate(String releaseDate) throws DateNotFoundException {
		List<Patient> patientByReleaseDate = null;
		patientByReleaseDate = collection.find(Filters.eq("releaseDate", releaseDate)).into(new ArrayList<Patient>())
				.stream().sorted((obj1, obj2)->obj1.getPatientName().compareTo(obj2.getPatientName()))
				.collect(Collectors.toList());
		if (patientByReleaseDate == null || patientByReleaseDate.isEmpty())
			throw new DateNotFoundException("No Data Found for the Date : " + releaseDate + ".");
		else {
			return patientByReleaseDate;
		}
	}

	@Override
	public List<Patient> findByContactNo(String contactNo) throws ContactNotFoundException {
		List<Patient> patientByContactNo = null;
		patientByContactNo = collection.find(Filters.eq("contactNo", contactNo)).into(new ArrayList<Patient>())
				.stream().sorted((obj1, obj2)->obj1.getPatientName().compareTo(obj2.getPatientName()))
				.collect(Collectors.toList());
		if (patientByContactNo == null || patientByContactNo.isEmpty())
			throw new ContactNotFoundException("No patient date is found with contact no. :(" + contactNo + ").");
		else {
			return patientByContactNo;
		}

	}

	@Override
	public Patient findByAadharNumber(String aadharNumber) throws AadharNumberNotFoundException {
		Patient patient = null;
		patient = collection.find(Filters.eq("_id", aadharNumber)).first();
		if (patient == null)
			throw new AadharNumberNotFoundException("Patient with given AadharNumber(" + aadharNumber + ") not found!");
		else {
			patient = collection.find(Filters.eq("_id", aadharNumber)).first();
		}
		return patient;
	}
}
