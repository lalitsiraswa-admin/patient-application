package com.patientapp.repository;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.patientapp.model.Patient;

public class DbManager {
	static MongoClient mongoInstance;

	public static void openConnection() {
		String conString = "mongodb://localhost:27017";
		ConnectionString connectionString = new ConnectionString(conString);
		CodecRegistry pojoCodecRegistry = fromProviders(PojoCodecProvider.builder().automatic(true).build());
		CodecRegistry codecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), pojoCodecRegistry);
		MongoClientSettings clientSettings = MongoClientSettings.builder().applyConnectionString(connectionString)
				.codecRegistry(codecRegistry).build();
		mongoInstance = MongoClients.create(clientSettings);
	}

	public static void closeConnection() {
		mongoInstance.close();
	}

	private MongoDatabase getDatabase() {
		MongoDatabase database = mongoInstance.getDatabase("kloudb");
		return database;
	}
	public MongoCollection<Patient> getCollection(){
		MongoDatabase database = getDatabase();
		MongoCollection<Patient> collection = database.getCollection("patient", Patient.class);
		return collection;
	}
}
