package com.patientapp.repository;

import java.util.List;

import com.patientapp.exceptions.AadharNumberNotFoundException;
import com.patientapp.exceptions.ContactNotFoundException;
import com.patientapp.exceptions.DateNotFoundException;
import com.patientapp.exceptions.DiseaseNotFoundException;
import com.patientapp.exceptions.DoctorNotFoundException;
import com.patientapp.exceptions.PatientBedNumberNotFoundException;
import com.patientapp.model.Patient;

public interface IPatientRepository {
	void addPatient(Patient patient);

	void updatePatient(String aadharNumber, String mobileno) throws AadharNumberNotFoundException;

	void deletePatient(String aadharNumber) throws AadharNumberNotFoundException;

	List<Patient>  findByBedNo(Integer patientBedNo) throws PatientBedNumberNotFoundException;

	List<Patient> getAllPatients();

	List<Patient> findByDisease(String disease) throws DiseaseNotFoundException;

	List<Patient> findByDoctorConsideration(String doctorName) throws DoctorNotFoundException;

	List<Patient> findByAdmitDate(String admitDate) throws DateNotFoundException;

	List<Patient> findByReleaseDate(String releaseDate) throws DateNotFoundException;

	List<Patient> findByContactNo(String contactNo) throws ContactNotFoundException;
	
	Patient findByAadharNumber(String aadharNumber) throws AadharNumberNotFoundException;

}
