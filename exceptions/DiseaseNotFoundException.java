package com.patientapp.exceptions;

public class DiseaseNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DiseaseNotFoundException() {
		
	}

	public DiseaseNotFoundException(String message) {
		super(message);
	}
}
