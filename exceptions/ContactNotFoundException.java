package com.patientapp.exceptions;

public class ContactNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ContactNotFoundException() {
	}

	public ContactNotFoundException(String message) {
		super(message);
	}
}
