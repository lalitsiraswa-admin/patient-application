package com.patientapp.exceptions;

public class DateNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DateNotFoundException() {
	}

	public DateNotFoundException(String message) {
		super(message);
	}
}
