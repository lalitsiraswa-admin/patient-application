package com.patientapp.exceptions;

public class AadharNumberNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AadharNumberNotFoundException() {
	}

	public AadharNumberNotFoundException(String message) {
		super(message);
	}
}
