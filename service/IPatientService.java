package com.patientapp.service;

import java.util.List;

import com.patientapp.exceptions.AadharNumberNotFoundException;
import com.patientapp.exceptions.ContactNotFoundException;
import com.patientapp.exceptions.DateNotFoundException;
import com.patientapp.exceptions.DiseaseNotFoundException;
import com.patientapp.exceptions.DoctorNotFoundException;
import com.patientapp.exceptions.PatientBedNumberNotFoundException;
import com.patientapp.model.Patient;

public interface IPatientService {

	void addPatient(Patient patient);

	void updatePatient(String aadharNumber, String mobileno) throws AadharNumberNotFoundException;

	void deletePatient(String aadharNumber) throws AadharNumberNotFoundException;

	List<Patient>  getByBedNo(Integer patientBedNo) throws PatientBedNumberNotFoundException;

	List<Patient> getAllPatients();

	List<Patient> getByDisease(String disease) throws DiseaseNotFoundException;

	List<Patient> getByDoctorConsideration(String doctorName) throws DoctorNotFoundException;

	List<Patient> getByAdmitDate(String admitDate) throws DateNotFoundException;

	List<Patient> getByReleaseDate(String releaseDate) throws DateNotFoundException;

	List<Patient> getByContactNo(String contactNo) throws ContactNotFoundException;
	
	Patient getByAadharNumber(String aadharNumber) throws AadharNumberNotFoundException;
}
