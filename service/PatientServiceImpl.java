package com.patientapp.service;

import java.util.List;

import com.patientapp.exceptions.AadharNumberNotFoundException;
import com.patientapp.exceptions.ContactNotFoundException;
import com.patientapp.exceptions.DateNotFoundException;
import com.patientapp.exceptions.DiseaseNotFoundException;
import com.patientapp.exceptions.DoctorNotFoundException;
import com.patientapp.exceptions.PatientBedNumberNotFoundException;
import com.patientapp.model.Patient;
import com.patientapp.repository.PatientRepositoryImpl;

public class PatientServiceImpl implements IPatientService {
	private PatientRepositoryImpl patientRepositoryImpl = new PatientRepositoryImpl();

	@Override
	public void addPatient(Patient patient) {
		patientRepositoryImpl.addPatient(patient);
	}

	@Override
	public void updatePatient(String aadharNumber, String mobileno) throws AadharNumberNotFoundException{
		patientRepositoryImpl.updatePatient(aadharNumber, mobileno);
	}

	@Override
	public void deletePatient(String aadharNumber) throws AadharNumberNotFoundException{
		patientRepositoryImpl.deletePatient(aadharNumber);
	}

	@Override
	public List<Patient>  getByBedNo(Integer patientBedNo) throws PatientBedNumberNotFoundException{
		
		return patientRepositoryImpl.findByBedNo(patientBedNo);
	}

	@Override
	public List<Patient> getAllPatients() {
		return patientRepositoryImpl.getAllPatients();
	}

	@Override
	public List<Patient> getByDisease(String disease) throws DiseaseNotFoundException{
		return patientRepositoryImpl.findByDisease(disease);
	}

	@Override
	public List<Patient> getByDoctorConsideration(String doctorName) throws DoctorNotFoundException{
		return patientRepositoryImpl.findByDoctorConsideration(doctorName);
	}

	@Override
	public List<Patient> getByAdmitDate(String admitDate) throws DateNotFoundException{
		return patientRepositoryImpl.findByAdmitDate(admitDate);
	}

	@Override
	public List<Patient> getByReleaseDate(String releaseDate) throws DateNotFoundException{
		return patientRepositoryImpl.findByReleaseDate(releaseDate);
	}

	@Override
	public List<Patient> getByContactNo(String contactNo) throws ContactNotFoundException{
		return patientRepositoryImpl.findByContactNo(contactNo);
	}

	@Override
	public Patient getByAadharNumber(String aadharNumber) throws AadharNumberNotFoundException{
		return patientRepositoryImpl.findByAadharNumber(aadharNumber);
	}

}
