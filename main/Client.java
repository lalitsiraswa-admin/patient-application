package com.patientapp.main;

import com.patientapp.repository.DbManager;
import com.patientapp.service.IPatientService;
import com.patientapp.service.PatientServiceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.mongodb.internal.connection.ClusterDescriptionHelper.Predicate;
import com.patientapp.exceptions.AadharNumberNotFoundException;
import com.patientapp.exceptions.ContactNotFoundException;
import com.patientapp.exceptions.DateNotFoundException;
import com.patientapp.exceptions.DiseaseNotFoundException;
import com.patientapp.exceptions.DoctorNotFoundException;
import com.patientapp.exceptions.PatientBedNumberNotFoundException;
import com.patientapp.model.Patient;

public class Client {

	public static void main(String[] args) throws AadharNumberNotFoundException, 
	ContactNotFoundException, DateNotFoundException, DiseaseNotFoundException,
	DoctorNotFoundException, PatientBedNumberNotFoundException{			
		DbManager.openConnection();
//		// "YYYY-MM-DD"
//		Patient patient = new Patient("Radhika", "987563546574", 73, "Dr.Rahul", "StomatchPain", "2022-02-21", "2022-02-22", "8692536475");
//		patientService.addPatient(patient);
		IPatientService patientService = new PatientServiceImpl();
		try {
			System.out.println("* * * Records of all patients of Hospital * * *");
			patientService.getAllPatients().forEach(System.out::println);
			
			System.out.println("* * * Patient By Bed Number * * *");
			patientService.getByBedNo(90).forEach(System.out::println);
			
			System.out.println("* * * Patient By Disease * * *");
			patientService.getByDisease("StomatchPain").forEach(System.out::println);
			
			System.out.println("* * * Patient By Doctor Under * * *");
			patientService.getByDoctorConsideration("Dr.Raveer").forEach(System.out::println);
			
			System.out.println("* * * Patient By Admit Date * * *");
			patientService.getByAdmitDate("2022-02-21").forEach(System.out::println);
			
			System.out.println("* * * Patient By Release Date * * *");
			patientService.getByReleaseDate("2011-09-20").forEach(System.out::println);
			
			System.out.println("* * * Patient By Contact Number * * *");
			patientService.getByContactNo("8675836475").forEach(System.out::println);
			
			System.out.println("* * * Patient By Aadhar Number * * *");
			System.out.println(patientService.getByAadharNumber("877563546576"));

			System.out.println("* * * Patient Data Update * * *");
			patientService.updatePatient("768947586735", "8793546578");

			System.out.println("* * * Patient Data Deleted * * * ");
			patientService.deletePatient("877563546576");

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}		
	}
}
